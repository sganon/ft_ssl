/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_parser.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sganon <sganon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/28 23:00:54 by sganon            #+#    #+#             */
/*   Updated: 2018/07/28 23:59:28 by sganon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void md5_parser(t_ssl *ssl, char **argv)
{
  (void)ssl;
  (void)argv;
  ft_putstr("Hello world from md5 parser\n");
}